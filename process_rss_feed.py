#!/usr/bin/python3

from urllib.parse import urlparse
import asyncio
import json
import os.path
import random
import signal
import subprocess
import sys

import aiohttp


with open('links.txt') as links_file:
    links = links_file.readlines()
    links = [link.strip() for link in links]


async def download_mp3(link: str):
    """
    Download the given link, turn it into an ogg, and test it.

    Args:
        link: The link you want to download.
    """
    filename = urlparse(link).path.split('/')[-1]

    async with download_concurrency_semaphore:
        if not os.path.exists(filename):
            async with aiohttp.ClientSession() as session:
                async with session.get(link) as response:
                    print(f'Downloading {link}')

                    with open(filename, 'wb') as the_file:
                        the_file.write(await response.read())

                if response.status != 200:
                    print(await response.text())
                    raise Exception(
                        'Non-200 status code: {} ({})'.format(response.status, link))
        else:
            print(f'Pretend downloading {filename}.')
            await asyncio.sleep(random.randint(0, 5))

    print(f'\tSaved as: "{filename}"')
    print(f'Successfully downloaded {link}')

    await process_mp3(filename)


async def process_mp3(mp3_filename: str):
    async with processor_concurrency_semaphore:
        print(f'Processing {mp3_filename}')
        name, extension = os.path.splitext(mp3_filename)
        ogg_filename = f'{name}.ogg'
        command = ['ffmpeg', '-nostats', '-loglevel', '8', '-i', mp3_filename, ogg_filename]
        print(f"Running {' '.join(command)}")
        process = await asyncio.create_subprocess_exec(*command)

        stdout, stderr = await process.communicate()

        if process.returncode != 0:
            raise Exception(f'Non-0 exit code: {process.returncode}')

        print(f'Successfully processed {mp3_filename}')

    await test_media(ogg_filename)


async def test_media(filename: str):
    async with audio_concurrency_semaphore:
        print(f'Playing {filename}')
        command = ['mpv', '--no-terminal', '--start', '33%', '--length', '5', filename]
        print(f"Running {' '.join(command)}")
        try:
            process = await asyncio.create_subprocess_exec(
                *command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            stdout, stderr = await asyncio.wait_for(process.communicate(), 4)

            if process.returncode:
                print(stdout)
                raise Exception(f'Non-0 exit code: {process.returncode} ({command})')
        except asyncio.TimeoutError:
            # mpv does not always exit after 10 seconds on some files. That's OK as long as we hear
            # audio.
            process.send_signal(signal.SIGINT)

            stdout, stderr = await process.communicate()

        print(f'Successfully tested {filename}')


async def main():
    global audio_concurrency_semaphore
    global download_concurrency_semaphore
    global processor_concurrency_semaphore
    audio_concurrency_semaphore = asyncio.Semaphore(value=1)
    download_concurrency_semaphore = asyncio.Semaphore(value=6)
    processor_concurrency_semaphore = asyncio.Semaphore(value=4)

    tasks = [download_mp3(link) for link in links]

    await asyncio.gather(*tasks)


asyncio.run(main(), debug=True)
